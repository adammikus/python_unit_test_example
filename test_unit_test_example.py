__author__ = 'Adam Mikus'

import unittest
import os
import sys

    
from pyspark import SparkConf, SparkContext

from pyspark.sql import SQLContext

from pyspark.sql.types import StructField, StructType, StringType, FloatType, DoubleType, IntegerType

import pandas as pd

from unit_test_example import PartClean, SimpleSparkJob

os.environ["SPARK_HOME"] = "/opt/cloudera/parcels/CDH/lib/spark"
    
class TestNormalizingPartNumber( unittest.TestCase ):
    """
        Simple test class for testing part cleaning function, which removes specific characters from string input (mpn).
        It is testing six cases:
        - Proper mpn string as input which needs to be cleaned.
        - Proper mpn string (only digits) as input which NO need to be cleaned
        - Integer as input (in some cases mpns are only digits, it should not throw an error if this kind of input presented as integer).
        - List of parts as input.
        - None as input.
        - Empty string as input.
    """
    
    def test_with_single_part( self ):
        ''' Test part cleaning with right string input ("I3-4170/SR.1P,L"), it should return I34170SR1PL '''
        result = PartClean.normalize_part( "I3-4170/SR.1P,L" )
        self.assertEqual( result, "I34170SR1PL" )
        
        
    def test_with_digits_string_as_input( self ):
        ''' Test part cleaning with an mpn consits only digits as string input, it should return the given input with no modification  '''
        result = PartClean.normalize_part( "133184" )
        self.assertEqual( result, "133184" )
        
    def test_with_digits_integer_as_input( self ):
        ''' Test part cleaning with an mpn consits only digits as integer input, it should return None and not raise an exception  '''
        result = PartClean.normalize_part( 133184 )
        self.assertIsNone( result )
        
        
    def test_with_multiple_parts( self ):
        ''' Test part cleaning with array of parts as input, it should return None since this function only cleans single part (string input) '''
        result = PartClean.normalize_part( ["I3-4170/SR.1P,L", "133184"] )
        self.assertIsNone( result )
        
    
        
    def test_with_none_as_input( self ):
        ''' Test part cleaning with None as input, it should return None and not raise an exception '''
        result = PartClean.normalize_part( None )
        self.assertIsNone( result )
        
        
    def test_with_empty_string_as_input( self ):
        ''' Test part cleaning with empty string as input, it should return None and not an empty string '''
        result = PartClean.normalize_part( ' ' )
        self.assertIsNone( result )
        

class ReusedPySparkTestCase( unittest.TestCase ):
    """
        Test class for creating reusable spark context for Pyspark test cases.
    """
    
    @classmethod
    def quiet_logs( cls ):
        ''' Function to reduce spark logs '''
        logger = cls.sc._jvm.org.apache.log4j
        logger.LogManager.getLogger("org"). setLevel( logger.Level.ERROR )
        logger.LogManager.getLogger("akka").setLevel( logger.Level.ERROR )

    
    @classmethod
    def setUpClass( cls ):
        """
        Set up class:
        - Initialize Spark context.
        - Initialize Hive context.
        - Load test dataset
        """
        conf = SparkConf().setMaster("local").setAppName('Test shortage etl app')
        cls.sc = SparkContext(conf=conf)
        cls.quiet_logs( )
        
        sqlContext = SQLContext( cls.sc )
        
        #Load test dataset
        df_pd = SimpleSparkJob._load_csv( SimpleSparkJob.SHORTAGE_FILE )
        cls.df_spark = SimpleSparkJob._convert_to_spark_df( sqlContext, df_pd, SimpleSparkJob.SHORTAGE_SCHEMA )
        


    @classmethod
    def tearDownClass( cls ):
        #Stop spark context
        cls.sc.stop()
        

class TestFilterAndResponse( ReusedPySparkTestCase ):
        """
        Simple test class for testing Pyspark rdd function wheather it maps data to the correct response format.
        It should return a list of parts in dictionary/dictionaries of id, (mpn-mfr), mpn, mfr and shortage_score rounded to 2 digits from the test dataset.
        It is testing three cases:
        - Amphenol as manufacturer, which presents in the test dataset.
        - TI as manufacturer, which does NOT present in the test dataset.
        - None as manufacturer.
        """
        
        expected_response = [
            { 'mpn': '031-6527', 'mfr': 'Amphenol', 'id': '031-6527-Amphenol', 'shortage_score': 3.98 },
            { 'mpn': '03M6142', 'mfr': 'Amphenol', 'id': '03M6142-Amphenol', 'shortage_score': 1.19 },
            { 'mpn': '03M8045', 'mfr': 'Amphenol', 'id': '03M8045-Amphenol', 'shortage_score': 1.19 },
            { 'mpn': '03M9134', 'mfr': 'Amphenol', 'id': '03M9134-Amphenol', 'shortage_score': 1.19 },
            { 'mpn': '03M9255', 'mfr': 'Amphenol', 'id': '03M9255-Amphenol', 'shortage_score': 1.19 },
            { 'mpn': '03M9772', 'mfr': 'Amphenol', 'id': '03M9772-Amphenol', 'shortage_score': 1.19 },
            { 'mpn': '03P3191', 'mfr': 'Amphenol', 'id': '03P3191-Amphenol', 'shortage_score': 1.19 }
        ]



        def test_with_correct_mfr_in_dataset( self ):
            ''' Test with Amphenol as manufacturer, it should return the defined expected_response list '''
            result = SimpleSparkJob._get_parts_by_mfr( self.df_spark, 'mpn', 'mfr', 'shortage_score', 'Amphenol' ).collect()
            self.assertItemsEqual( result, self.expected_response )
            
            
        def test_with_mfr_not_in_dataset( self ):
            ''' Test with TI as manufacturer, it should return an empty list since no TI part in test dataset '''
            result = SimpleSparkJob._get_parts_by_mfr( self.df_spark, 'mpn', 'mfr', 'shortage_score', 'TI' ).collect()
            self.assertEqual( len(result), 0 )
            
            
        def test_with_none_as_mfr( self ):
            ''' Test with None as manufacturer, it should return an empty list '''
            result = SimpleSparkJob._get_parts_by_mfr( self.df_spark, 'mpn', 'mfr', 'shortage_score', None ).collect()
            self.assertEqual( len(result), 0 )
                