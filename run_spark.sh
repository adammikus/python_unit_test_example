export PYSPARK_PYTHON=/opt/local/anaconda/bin/python

export PYTHONPATH=/opt/cloudera/parcels/CDH/lib/spark/python/:$PYTHONPATH

export PYTHONPATH=/opt/cloudera/parcels/CDH/lib/spark/python/lib/py4j-0.9-src.zip:$PYTHONPATH

SPARK_CLASSPATH="/opt/cloudera/parcels/CDH-5.12.0-1.cdh5.12.0.p0.29/lib/hbase/lib:/opt/cloudera/parcels/CDH-5.12.0-1.cdh5.12.0.p0.29/lib/hbase/lib/hbase-common-1.2.0-cdh5.12.0.jar:/opt/cloudera/parcels/CDH-5.12.0-1.cdh5.12.0.p0.29/lib/hbase/lib/hbase-client-1.2.0-cdh5.12.0.jar:/opt/cloudera/parcels/CDH-5.12.0-1.cdh5.12.0.p0.29/lib/hbase/lib/hbase-protocol-1.2.0-cdh5.12.0.jar:/opt/cloudera/parcels/CDH-5.12.0-1.cdh5.12.0.p0.29/lib/hbase/lib/guava-12.0.1.jar:/opt/cloudera/parcels/CDH-5.12.0-1.cdh5.12.0.p0.29/lib/spark/lib/spark-examples-1.6.0-cdh5.12.0-hadoop2.6.0-cdh5.12.0.jar" spark-submit --master yarn --num-executors 6 --executor-cores 6 --executor-memory 32g --driver-memory 32g --conf spark.yarn.executor.memoryOverhead=8192 $1
