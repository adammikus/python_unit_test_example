import sys
import re
import time


from pyspark import SparkConf, SparkContext

from pyspark.sql import SQLContext

from pyspark.sql.types import StructField, StructType, StringType, FloatType, DoubleType, IntegerType

import pandas as pd

class PartClean:
    
    @staticmethod
    def normalize_part( mpn ):    
        '''Remove special characters from mpn'''
        try:
            #If empty string return None
            if mpn.strip() == '':
                return None
            
            return re.sub('\W+','', mpn)
        except:
            return None


class SimpleSparkJob:
    """
        A simple spark job for pyspark unit test example.
        The job loads a csv dataset extract from shortage_final table and filters for a specific manufacturer ( in this example TE Connectivity Ltd ) and returns a transformed dataset (list of dictionaries) from the csv extract.
    """
    
    SHORTAGE_FILE = 'test_file.csv'
    
    SHORTAGE_SCHEMA = StructType([ StructField("mpn", StringType(), True)\
                       ,StructField("mfr", StringType(), True)\
                       ,StructField("score_date", StringType(), True)\
                       ,StructField("shortage_score", FloatType(), True)\
                       ,StructField("lead_time_average_feature", DoubleType(), True)\
                       ,StructField("lead_time_history_feature", DoubleType(), True)\
                       ,StructField("inventory_binary_feature", DoubleType(), True)\
                       ,StructField("inventory_history_feature", FloatType(), True)\
                       ,StructField("quotes_industry_feature", IntegerType(), True)\
                       ,StructField("number_of_non_null_features", IntegerType(), True)\
                       ,StructField("tax_level_1", StringType(), True)
                       ,StructField("tax_level_2", StringType(), True)
                       ,StructField("tax_level_1", StringType(), True)])
    
    
    @staticmethod
    def _load_csv( file ):
        '''Load csv file by pandas library'''
        return pd.read_csv( file, low_memory=False )
    
    
    @staticmethod
    def _map_to_dict( mpn, mfr, shortage_score ):
        """
        Private static function to map into data to dictionary.
        Creates an extra id attribute based on mpn and mfr input.
        :param mpn: String, mpn.
        :param mfr: String, manufacturer.
        :param shortage_score: Float, shortage score.
        :return: dictionary, attributes: { 'id', 'mpn', 'mfr', 'shortage_score' }
        """
        shortage_dict = {
            'id' : ( mpn + '-' + mfr ),
            'mpn' : mpn,
            'mfr' : mfr,
            'shortage_score' : shortage_score
        }

        return shortage_dict
        
    @staticmethod
    def _get_parts_by_mfr( df, mpn_col, mfr_col, shortage_score_col, mfr ):
        """
        Private static function to filter spark dataframe based on one specific manufacturer anme and return an rdd of id, mpn, mfr and shortage score.
        :param df: Spark dataframe, dataframe to operate on.
        :param mpn_col: String, name of the column in the spark dataframe representing mpn.
        :param shortage_score: Float, shortage score.
        :return: dictionary, attributes: { 'id', 'mpn', 'mfr', 'shortage_score' }
        """
        return df.filter( df[ mfr_col ] == mfr ).map(lambda x: SimpleSparkJob._map_to_dict( x[ mpn_col ], x[ mfr_col ], round( x[ shortage_score_col ], 2 ) ) )
    
    @staticmethod
    def _convert_to_spark_df( sqlContext, df_pd, schema ):
        '''Convert pandas dataframe to spark dataframe based on predefined schema'''
        return sqlContext.createDataFrame( df_pd, schema=schema )

    
    @staticmethod
    def analyze( sc ):
        sqlContext = SQLContext( sc )  
        #Load shortage table
        df_pd = SimpleSparkJob._load_csv( SimpleSparkJob.SHORTAGE_FILE )
        df_spark = SimpleSparkJob._convert_to_spark_df( sqlContext, df_pd, SimpleSparkJob.SHORTAGE_SCHEMA )
        
        #Filter for TE Connectivity Ltd and print results
        print( SimpleSparkJob._get_parts_by_mpn( df_spark, 'mpn', 'mfr', 'shortage_score', 'TE Connectivity Ltd' ).collect() )
        
        

if __name__ == "__main__":
    '''Main executor function of script'''
    
    #Configure Spark
    conf = SparkConf().setAppName("Simple job for unit test example")
    
    sc = SparkContext( conf=conf )

    #Execute Main functionality

    start = time.time()
    
    SimpleSparkJob.analyze( sc )
   
    end = time.time()
    print("\n The execution of the program took %s seconds" % (end-start))